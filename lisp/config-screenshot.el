;;; config-screenshot.el --- Configuration for screenshot.el -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "screenshot" '(require 'screenshot))

(require 'config)

;;;###autoload
(config-install-package 'screenshot)

(defvar screenshot-border-width)        ; Defined in screenshot.el.
(defvar screenshot-shadow-offset-horizontal) ; Defined in screenshot.el.
(defvar screenshot-shadow-offset-vertical) ; Defined in screenshot.el.
(defvar screenshot-shadow-radius) ; Defined in screenshot.el.

(setq screenshot-border-width 0)
(setq screenshot-shadow-offset-horizontal 0)
(setq screenshot-shadow-offset-vertical 0)
(setq screenshot-shadow-radius 0)

(defvar flyspell-mode)
(defvar jinx-mode)
(defvar show-smartparens-mode)

(declare-function flyspell-mode "ext:flyspell")
(declare-function jinx-mode "ext:jinx")
(declare-function show-smartparens-mode "ext:smartparens")

(defun config-screenshot-setup-buffer ()
  (if (and (featurep 'flyspell) flyspell-mode )
      (flyspell-mode -1))
  (if (and (featurep 'jinx) jinx-mode)
      (jinx-mode -1))
  (if (and (featurep 'show-paren-mode) show-paren-mode)
      (show-paren-mode -1))
  (if (and (featurep 'smartparens) show-smartparens-mode)
      (show-smartparens-mode -1)))

(add-hook 'screenshot-buffer-creation-hook #'config-screenshot-setup-buffer)

;;; Footer:

(provide 'config-screenshot)

;;; config-screenshot.el ends here
