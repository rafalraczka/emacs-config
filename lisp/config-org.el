;;; config-org.el --- Org configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "org" '(require 'config-org))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'citar)

;;;###autoload
(config-install-package 'org)

;;;###autoload
(config-install-package 'org-contrib)

;;;###autoload
(config-install-package 'org-noter)

;;;###autoload
(config-install-package 'org-pdftools)

;;;###autoload
(config-install-package 'org-ql)

;;;###autoload
(config-install-package 'org-ref-prettify)

;;;###autoload
(config-install-package 'org-transclusion)

;;;###autoload
(config-install-package 'org-tree-slide)

(require 'org)
(require 'oc-csl)

(cl-eval-when (eval load)
  (require 'citar))

(eval-when-compile
    (require 'ob-ditaa)
    (require 'ox))

(defcustom config-org-fill-column 72
  "Default `fill-column' for Org mode."
  :type 'integer)

(defun config-org-mode-config ()
  (setq fill-column config-org-fill-column))

(add-hook 'org-clock-cancel-hook #'save-buffer)
(add-hook 'org-clock-in-hook #'save-buffer)
(add-hook 'org-clock-out-hook #'save-buffer)
(add-hook 'org-mode-hook #'auto-fill-mode)
(add-hook 'org-mode-hook #'config-org-mode-config)
(add-hook 'org-mode-hook #'visual-line-mode)
(add-hook 'org-timer-set-hook #'org-clock-in)

(setq org-cite-activate-processor 'citar)
(setq org-cite-follow-processor 'citar)
(setq org-cite-insert-processor 'citar)
(setq org-default-priority ?C)
(setq org-edit-src-content-indentation 0)
(setq org-ellipsis "…")
(setq org-export-in-background t)
(setq org-fold-catch-invisible-edits 'smart)

;; Hide all leading stars in a heading except the last one.

(setq org-hide-leading-stars t)
(setq org-highest-priority ?A)

;; Use id property when storing links to org entries and id property is
;; present.

(setq org-id-link-to-org-use-id 'use-existing)

(setq org-lowest-priority ?E)
(setq org-outline-path-complete-in-steps nil)
(setq org-refile-targets '((nil :maxlevel . 8)
                           (org-agenda-files :maxlevel . 1)))
(setq org-refile-use-outline-path t)
(setq org-startup-folded 'unfold)

(push '(".+\\.org-.*"
        (display-buffer-in-direction)
        (direction . right)
        (window-width . 0.33))
      display-buffer-alist)

(org-babel-do-load-languages
 'org-babel-load-languages
 `(,@org-babel-load-languages
   (C . t)
   (R . t)
   (julia . t)
   (latex . t)
   (lilypond . t)
   (python . t)
   (shell . t)
   (sql . t)))

(when-let* ((path "/usr/share/java/ditaa/ditaa-0.11.jar")
            ((file-exists-p path))
            ((setq org-ditaa-jar-path path))
            (path-eps "/usr/share/java/ditaa-eps/DitaaEps.jar")
            ((file-exists-p path-eps)))
  (setq org-ditaa-eps-jar-path path-eps)
  (org-babel-do-load-languages 'org-babel-load-languages
                               `(,@org-babel-load-languages (ditaa . t))))


;;; Extra link types.

(require 'ol-git-link)


;;; org-noter.

(require 'org-noter)

(setf org-noter-always-create-frame nil)

(setf org-noter-auto-save-last-location t)

(setf org-noter-doc-split-fraction '(0.6 0.4))

(setf org-noter-kill-frame-at-session-end nil)


;;; org-pdftools.

(declare-function org-pdftools-setup-link "ext:org-pdftools")

(org-pdftools-setup-link)


;;; org-transclusion.

(defvar org-transclusion-exclude-elements)


(setf org-transclusion-exclude-elements
      '(clock drawer planning property-drawer planning))


(declare-function org-transclusion-mode "org-transclusion")

(add-hook 'org-mode-hook #'org-transclusion-mode)


;;; org-tree-slide.

(defvar-local config-org-tree-slide--original-var-values nil)

(defcustom config-org-tree-slide-minor-modes
  '((:disable blink-cursor-mode
              display-line-numbers-mode
              flyspell-mode)
    (:enable config-org-tree-slide-face-mode
             org-indent-mode
             text-scale-mode
             variable-pitch-mode
             visual-fill-column-mode
             visual-fill-mode
             visual-line-mode))
    "Modes which will be disabled or enabled with `org-tree-sldie-mode'.
Modes after disable keyword will be disabled on
`org-tree-slide-mode' enabling and disabled on its disabling.
The process will be reversed for modes after the enable keyword.")

(defcustom config-org-tree-slide-temp-faces
  `((org-drawer (:inherit org-hide))
    (org-level-1 (:height 1.2 :inherit org-level-1))
    (org-headline-done (:inherit unspecified))
    (org-table (:family "JuliaMono" :inherit nil))
    (org-tag (:inherit org-hide))
    (org-tree-slide-header-overlay-face
     (:height 1.4 :inherit org-document-title)))
  "")

(defcustom config-org-tree-slide-temp-variables
  '((line-spacing 5)
    (text-scale-mode-amount 2)
    (org-indent-indentation-per-level 0)
    (visual-fill-column-width 40)
    (visual-line-fringe-indicators nil)
    (visual-fill-column-center-text t)
    (org-hide-emphasis-markers t))
  "")

(defvar config-org-tree-slide-face-mode)

(define-minor-mode config-org-tree-slide-face-mode
  "Variable-pitch default-face mode.
An interface to `buffer-face-mode' which uses the `variable-pitch' face.
Besides the choice of face, it is the same as `buffer-face-mode'."
  :variable config-org-tree-slide-face-mode
  (let ((faces (when config-org-tree-slide-face-mode
                 config-org-tree-slide-temp-faces)))
    (setq-local face-remapping-alist faces)))

(defvar org-tree-slide-mode)

(defun config-org-tree-slide-toggle-minor-modes ()
  (dolist (list config-org-tree-slide-minor-modes)
    (let ((enable (eq (car list) :enable))
          (modes (cdr list)))
      (unless org-tree-slide-mode
        (setq enable (not enable)))
      (dolist (mode modes)
        (when (functionp mode)
          (if enable
              (funcall mode +1)
            (funcall mode -1)))))))

(defun config-org-tree-slide-set-variables ()
  (dolist (variable config-org-tree-slide-temp-variables)
    (let ((var (car variable))
          (val (car (cdr variable))))
      (if org-tree-slide-mode
          (progn
            (push (list var (symbol-value var))
                  config-org-tree-slide--original-var-values)
            (set-variable var val t))
        (let ((old-val (car (cdr (assoc var config-org-tree-slide--original-var-values)))))
          (set-variable var old-val t)))))
  (unless org-tree-slide-mode
    (setq config-org-tree-slide--original-var-values nil)))

(defun config-org-tree-slide-enable ()
  (org-link-preview-region))

(add-hook 'org-tree-slide-mode-hook
          #'config-org-tree-slide-toggle-minor-modes 20)
(add-hook 'org-tree-slide-mode-hook #'config-org-tree-slide-set-variables 10)
(add-hook 'org-tree-slide-play-hook #'config-org-tree-slide-enable)
(add-hook 'org-tree-slide-play-hook #'org-link-preview-region)
(add-hook 'org-tree-slide-stop-hook #'org-link-preview-clear)

(defvar org-tree-slide-slide-in-effect)

(setq org-tree-slide-slide-in-effect nil)


;;; Keymaps.

;;;; org-mode-map.

(let ((map org-mode-map))
  (keymap-unset map "<tab>" t)
  (keymap-set map "C-<tab>" #'org-cycle)
  (keymap-set map "C-c c" #'org-cite-insert))

;;; Footer:

(provide 'config-org)

;;; config-org.el ends here
