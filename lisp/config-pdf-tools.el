;;; config-pdf-tools.el --- PDF viewer configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (when (display-graphic-p)

;;;###autoload (eval-after-load "pdf-view" '(require 'config-pdf-tools))

;;;###autoload
(require 'config)

;;;###autoload
(defvar config-pdf-tools-installed (locate-file "pdf-tools.el" load-path))

;;;###autoload
(unless config-pdf-tools-installed
  (config-install-package 'pdf-tools))

;;;###autoload
(require 'config-env)

(require 'pdf-annot)
(require 'pdf-isearch)
(require 'pdf-links)
(require 'pdf-view)

(declare-function 'pdf-annot-minor-mode "ext:pdf-annot")
(declare-function 'pdf-isearch-minor-mode "ext:pdf-isearch")
(declare-function 'pdf-links-minor-mode "ext:pdf-links")
(declare-function 'pdf-view-mode "ext:pdf-view")

(add-hook 'pdf-view-mode-hook #'pdf-annot-minor-mode)
(add-hook 'pdf-view-mode-hook #'pdf-isearch-minor-mode)
(add-hook 'pdf-view-mode-hook #'pdf-links-minor-mode)

;;;###autoload (autoload 'pdf-view-mode "pdf-view")

(declare-function straight--build-dir "straight")

;;;###autoload
(progn (defun config-pdf-tools-remove-straight-dir-from-load-path ()
  (setf load-path
        (remove (expand-file-name "pdf-tools" (straight--build-dir))
                load-path))))

;;;###autoload
(config-eval-after-init '(config-pdf-tools-remove-straight-dir-from-load-path))

(declare-function pdf-tools-install "ext:pdf-tools")

;;;###autoload
(if (or config-pdf-tools-installed
        (file-exists-p (expand-file-name
                        "epdfinfo" (straight--build-dir "pdf-tools"))))
    (progn
      (add-to-list 'auto-mode-alist
                   '("\\.[pP][dD][fF]\\'" . pdf-view-mode))
      (add-to-list 'magic-mode-alist
                   '("%PDF" . pdf-view-mode)))
  (pdf-tools-install 'no-query-p))

(setq-default pdf-view-display-size 'fit-page)

;;;###autoload )

;;; Footer:

(provide 'config-pdf-tools)

;;; config-pdf-tools.el ends here
