;;; config-lilypond.el --- Configuration for LilyPond -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (eval-after-load "lilypond-mode" '(require 'config-lilypond))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'lilypond)

;;;###autoload
(autoload 'lilypond-mode "lilypond-mode"
  "Major mode for editing LilyPond music files."
  'interactive)

;;;###autoload
(add-to-list 'auto-mode-alist '("\\.ly\\'" . LilyPond-mode))

;;; Footer:

(provide 'config-lilypond)

;;; config-lilypond.el ends here
