;;; config-util-tree-sitter.el --- Tree-sitter utility functions -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(defvar config-tree-sitter--current-enable-args nil)

(defvar config-tree-sitter--hooks '(find-file-hook post-command-hook))

(defun config-tree-sitter--enable-ts-mode--temp-hook ()
  (let ((mode (cadr config-tree-sitter--current-enable-args))
        (function (caddr config-tree-sitter--current-enable-args)))
    (setf config-tree-sitter--current-enable-args nil)
    (dolist (hook config-tree-sitter--hooks)
      (remove-hook hook 'config-tree-sitter--enable-ts-mode--temp-hook))
    (if (eq major-mode mode)
        (funcall function))))

;;;###autoload
(defun config-tree-sitter-maybe-enable (language &optional mode function)
  "Map MODE for a LANGUAGE to an alternative Tree-sitter mode, if available.
After the alternative mode has been mapped, change major mode in all
live buffers with `major-mode' MODE to the alternative mode.

MODE is a major mode which would be replaced with an alternative mode,
defaults to a LANGUAGE symbol with a -mode suffix.  For example, for
a LANGUAGE `python' default MODE value is `python-mode'.

FUNCTION is a function enabling alternative mode, defaults to a LANGUAGE
with a -ts-mode suffix.  For example, for a LANGUAGE `python' default
FUNCTION value is `python-ts-mode'.

MODE-HOOK is a hook run after entering MODE, defaults to a MODE with a
-hook suffix.  For example, for a MODE `python-mode' default MODE-HOOK
value is `python-mode-hook'."
  (when (treesit-language-available-p language)
    (unless mode
      (setf mode (intern (concat (symbol-name language) "-mode"))))
    (unless function
      (setf function (intern (concat (symbol-name language) "-ts-mode"))))
    (add-to-list 'major-mode-remap-alist (cons mode function))
    (setf config-tree-sitter--current-enable-args (list language mode function))
    (dolist (buffer (buffer-list))
      (with-current-buffer buffer
        (if (eq major-mode mode)
            (funcall function))))
    (when (eq major-mode 'fundamental-mode)
      (dolist (hook config-tree-sitter--hooks)
        (add-hook hook 'config-tree-sitter--enable-ts-mode--temp-hook)))))

;;; Footer:

(provide 'config-util-tree-sitter)

;;; config-util-tree-sitter.el ends here
