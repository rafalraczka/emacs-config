;;; config-project.el --- Project interaction -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (setf features (remove 'project features))
;;;###autoload (eval-after-load "project" '(require 'config-project))
;;;###autoload (eval-after-load "projectile" '(require 'config-project))

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'projectile)

(require 'cl-lib)
(require 'config-env)
(require 'projectile)

(declare-function straight--repos-dir "ext:straight")

(defun config-project-list-store-dirctories ()
  (let ((dirs (list config-env-repositories-directory)))
    (if (featurep 'straight)
        (push (straight--repos-dir) dirs))
    (nreverse dirs)))

;;;###autoload
(defun config-project-discover-projects ()
  "Search for all of the not known projects and remember them.
This function search non-recursively in all directories returned
by `config-project-list-store-dirctories'."
  (interactive)
  (project--ensure-read-project-list)
  (let ((dirs (reverse (config-project-list-store-dirctories)))
        (known (make-hash-table :size (* 2 (length project--list))
                                :test #'equal))
        (count 0))
    (dolist (project (mapcar #'car project--list))
      (puthash project t known))
    (dolist (dir dirs)
      (dolist (d (directory-files dir t directory-files-no-dot-files-regexp))
        (when-let* (((file-directory-p d))
                    (project (project--find-in-directory d))
                    (project-root (project-root project))
                    (not-new t))
          (unless (gethash project-root known)
            (project-remember-project project t)
            (puthash project-root t known)
            (setf not-new nil))
          (unless (member project-root projectile-known-projects)
            (projectile-add-known-project project-root)
            (when not-new (setf not-new t)))
          (unless not-new (cl-incf count)))))
    (unless (zerop count)
      (project--write-project-list))
    count))

(defvar project-vc-extra-root-markers)

(setf project-vc-extra-root-markers '(".stfolder"))

(add-to-list 'projectile-project-root-files ".stfolder")

(config-project-discover-projects)

(unless projectile-mode (projectile-mode))


;;; Compilation.

(setf compilation-scroll-output 'first-error)

(add-hook 'compilation-filter-hook 'ansi-color-compilation-filter)

;;; Footer:

(provide 'config-project)

;;; config-project.el ends here
