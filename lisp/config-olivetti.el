;;; config-olivetti.el --- Olivetti configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'olivetti)

(require 'olivetti)

;;;###autoload
(defvar-local config-olivetti--initial-body-width nil)

;;;###autoload
(progn (defun config-olivetti--max-line-width ()
  "Move to the ARGth next long line greater than `fill-column'."
  (save-excursion
    (save-restriction
      (widen)
      (goto-char (point-min))
      (let ((width (- (pos-eol) (pos-bol))))
        (while (zerop (forward-line))
          (let ((current-width (- (pos-eol) (pos-bol))))
            (if (> current-width width) (setf width current-width))))
        width)))))

;;;###autoload
(progn (defun config-olivetti-set-body-width ()
  "Set olivetti body width according to `fill-column' value."
  (if olivetti-mode
      (progn
        (if (and (not config-olivetti--initial-body-width)
                 (local-variable-p 'olivetti-body-width))
            (setf config-olivetti--initial-body-width olivetti-body-width))
        (let ((width (if (local-variable-p 'olivetti-body-width)
                         config-olivetti--initial-body-width
                       (max (config-olivetti--max-line-width)
                            (if (local-variable-p 'fill-column)
                                fill-column
                              80)))))
          (if (and (boundp 'display-line-numbers-mode)
                   display-line-numbers-mode)
              (let ((lines (count-lines (point-min) (point-max))))
                (setf width (+ (length (number-to-string (* lines 2)))
                               width))))
          (setf olivetti-body-width width)
          (setq-local olivetti-minimum-body-width width)))
    (if config-olivetti--initial-body-width
        (progn (setq-local olivetti-body-width
                           config-olivetti--initial-body-width)
               (setf config-olivetti--initial-body-width nil))
      (kill-local-variable 'olivetti-body-width)))))

;;;###autoload
(add-hook 'olivetti-mode-hook #'config-olivetti-set-body-width)

;;;###autoload
(setf olivetti-global-modes
      '(text-mode
        prog-mode
        conf-mode
        dired-mode
        (special-mode
         :exclude (sqlite-mode pdf-view-mode))))

(defvar olivetti-lighter)

;;;###autoload
(setf olivetti-lighter nil)

;;;###autoload (add-hook 'after-init-hook 'olivetti-global-mode)

(unless olivetti-global-mode (olivetti-global-mode))

;;; Footer:

;;;###autoload
(provide 'config-olivetti)

;;; config-olivetti.el ends here
