;;; config-env.el --- Modular configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;; System dependent variables -------

(defun config-env-android-p ()
  "Return t if current system is Android, and return nil otherwise."
  (string-equal "Android\n" (shell-command-to-string "uname -o")))

(defun config-env-gnu-linux-p ()
  "Return t if current system is GNU/Linux, and return nil otherwise."
  (eq system-type 'gnu/linux))

(defun config-env-windows-p ()
  "Return t if current system is Windows, and return nil otherwise."
  (eq system-type 'windows-nt))

(defconst config-env-android (config-env-android-p)
  "This constant specify whatever user is using Emacs on device with Android system.")

(defconst config-env-gnu-linux (config-env-gnu-linux-p)
  "This constant specify whatever user is using Emacs on device with GNU/Linux system.")

(defconst config-env-windows (config-env-windows-p)
  "This constant specify whatever user is using Emacs on device with Windows system.")

(defconst config-env-daemon (daemonp))

;;;;; Guix

(defvar config-env-guix-store-directory "/gnu/store/")

(defun config-env-guix-p ()
  "Return t if Emacs is installed with GNU Guix, and return nil otherwise."
  (and config-env-gnu-linux
       (file-directory-p config-env-guix-store-directory)
       (let ((regexp (concat "\\`" config-env-guix-store-directory)))
         (and (string-match-p regexp data-directory)
              (string-match-p regexp exec-directory)))
       t))

(defconst config-env-guix (config-env-guix-p)
  "This constant specify whatever Emacs is installed with GNU Guix.")

;;;;; Nix

(defvar config-env-nix-store-directory "/nix/store/")

(defun config-env-nix-p ()
  "Return t if Emacs is installed with Nix, and return nil otherwise."
  (and config-env-gnu-linux
       (file-directory-p config-env-nix-store-directory)
       (let ((regexp (concat "\\`" config-env-nix-store-directory)))
         (and (string-match-p regexp data-directory)
              (string-match-p regexp exec-directory)))
       t))

(defconst config-env-nix (config-env-nix-p)
  "This constant specify whatever Emacs is installed with Nix package manager.")

;;;; Paths ----------------------------

(defconst config-env-chemacs-directory
  (when-let ((file (car (rassoc '((require . chemacs)) load-history))))
    (file-name-directory file))
  "This constant specify directory from which chemacs.el has been required.
If Chemacs is not in use in current session the constant value is
nil.")

(defconst config-env-repositories-directory
  (expand-file-name "~/repos/")
  "Default directory where user stores files.")

;;;; Others ---------------------------

(defun config-env-exwm-require-p ()
  (and (or (member "--use-exwm" command-line-args)
           (member "(cond ((file-exists-p \"~/.exwm\") (load-file \"~/.exwm\")) ((not (featurep (quote exwm))) (require (quote exwm)) (require (quote exwm-config)) (exwm-config-default) (message (concat \"exwm configuration not found. \" \"Falling back to default configuration...\"))))"
                   command-line-args))
       (eq window-system 'x)))

(defconst config-env-exwm-required (config-env-exwm-require-p))

;;; Footer:

(provide 'config-env)

;;; config-env.el ends here
