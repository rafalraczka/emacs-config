;;; config-exwm.el --- EXWM configuration -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload (when config-env-exwm-required

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'exwm)

;;;###autoload   (require 'config-exwm)
;;;###autoload   (exwm-enable)
;;;###autoload   (exwm-randr-mode))

(require 'config)
(require 'exwm)
(require 'exwm-randr)

(defvar config-exwm-main-monitor-workspaces-number 1)

;;;; exwm

(defun config-exwm-mode-line-format ()
  (when (featurep 'doom-modeline)
    (setq-local doom-modeline-buffer-encoding nil)
    (setq-local doom-modeline-buffer-state-icon nil)
    (setq-local doom-modeline-major-mode-icon nil))
  (setq-local column-number-mode nil)
  (setq-local line-number-mode nil))

(defun config-exwm-run-in-background (command)
  (let ((command-parts (split-string command "[ ]+")))
    (when (executable-find (nth 0 command-parts))
      (apply #'call-process `(,(car command-parts)
                              nil 0 nil
                              ,@(cdr command-parts))))))

(add-hook 'exwm-mode-hook #'config-exwm-mode-line-format)

(setq exwm-input-prefix-keys
      `(
        ,@(number-sequence ?\C-\s-a ?\C-\s-z)
        ,@(number-sequence ?\s-a ?\s-z)
        ?\C-c ?\C-h ?\C-u ?\C-x
        ?\M-! ?\M-& ?\M-: ?\M-` ?\M-x
        ?\s-+
        f5 f6 f7 f8 f9
        ))

(setq exwm-input-simulation-keys
      `(([?\C-_] . [?\C-z])
        ([?\C-a] . [home])
        ([?\C-b] . [left])
        ([?\C-e] . [end])
        ([?\C-f] . [right])
        ([?\C-n] . [down])
        ([?\C-p] . [up])
        ([?\C-w] . [?\C-x])
        ([?\C-y] . [?\C-v])
        ([?\M-_] . [?\C-y])
        ([?\M-w] . [?\C-c])))

(setq exwm-layout-show-all-buffers t)

;;;; exwm-randr

(defun config-exwm-randr-generate-plist (&optional monitors)
  "Generate plist of MONITORS and their workspaces.
MONITORS is optional and should be in a form of the
`config-exwm-randr-list-monitors' output.  With MAX user can
define number of the last workspace, it is equal to
`exwm-workspace-number' when nil."
  (let* ((monitors (or monitors (config-exwm-randr-list-monitors)))
         (ws-num (+ (1- (length monitors))
                    config-exwm-main-monitor-workspaces-number))
         (cur-monitor-idx -1)
         (cur-ws 0)
         (cur-primary-monitor-ws-num 0)
         (repeat-monitor nil)
         cur-monitor
         cur-monitor-is-primary
         cur-monitor-name
         result)
    (when (> (length monitors) 10)
      (user-error "Number of monitors unsupported (larger than 10)."))
    (while (< cur-ws ws-num)
      (unless repeat-monitor
        (setf cur-monitor-idx (1+ cur-monitor-idx)
              cur-monitor (nth cur-monitor-idx monitors)
              cur-monitor-is-primary (gethash 'primary cur-monitor)
              cur-monitor-name (gethash 'name cur-monitor)))
      (setf result (plist-put result cur-ws cur-monitor-name))
      (when cur-monitor-is-primary
        (setf cur-primary-monitor-ws-num (1+ cur-primary-monitor-ws-num)))
      (setf repeat-monitor (and cur-monitor-is-primary
                                (< cur-primary-monitor-ws-num
                                   config-exwm-main-monitor-workspaces-number))
            cur-ws (1+ cur-ws)))
    result))

(defun config-exwm-randr-list-monitors ()
  (let* ((list (cdr (split-string
                     (shell-command-to-string "xrandr --listactivemonitors")
                     "\n" 'omit-nulls))))
    (mapcar #'config-exwm-randr-parse-monitor-list list)))

(defun config-exwm-randr-parse-monitor-list (string)
  (let ((result (make-hash-table :size 10))
        output)
    (string-match (concat "^ \\([0-9]+\\): "
                          "\\([+]\\{0,1\\}\\)\\([*]\\{0,1\\}\\)\\(.*?\\) "
                          "\\([0-9]+?\\)/\\([0-9]+?\\)x"
                          "\\([0-9]+?\\)/\\([0-9]+?\\)"
                          "\\+\\([0-9]+?\\)\\+\\([0-9]+?\\)[ ]\\{1,2\\}"
                          "\\(.*?\\)$")
                  string)
    (setf output (match-string 11 string))
    (puthash 'order (string-to-number (match-string 1 string)) result)
    (puthash 'primary (not (string-empty-p (match-string 3 string))) result)
    (puthash 'name (match-string 4 string) result)
    (puthash 'w (string-to-number (match-string 5 string)) result)
    (puthash 'wmm (string-to-number (match-string 6 string)) result)
    (puthash 'h (string-to-number (match-string 7 string)) result)
    (puthash 'hmm (string-to-number (match-string 8 string)) result)
    (puthash 'x (string-to-number (match-string 9 string)) result)
    (puthash 'y (string-to-number (match-string 10 string)) result)
    (puthash 'output (if (string-empty-p output) nil output) result)
    result))

;;;; exwm-workspace

(defun config-exwm-workspace-rename-buffer ()
  (exwm-workspace-rename-buffer (concat exwm-title "-exwm")))

(defun config-exwm-workspace-number ()
  (+ (1- (length (config-exwm-randr-list-monitors)))
     config-exwm-main-monitor-workspaces-number))

(defun config-exwm-workspace-adjust (&optional new-number old-number)
  (let* ((new-number (or new-number (config-exwm-workspace-number)))
         (old-number (or old-number (exwm-workspace--count)))
         (dif-number (- new-number old-number)))
    (cond
     ((eq 0 dif-number)
      (ignore))
     ((< 0 dif-number)
      (mapc #'exwm-workspace-add
            (number-sequence old-number (1- new-number))))
     ((> 0 dif-number)
      (mapc #'exwm-workspace-delete
            (reverse (number-sequence new-number (1- old-number))))))
    new-number))

(add-hook 'exwm-update-title-hook #'config-exwm-workspace-rename-buffer)

(setq exwm-workspace-show-all-buffers t)
(setq exwm-workspace-warp-cursor t)


;;; autorandr interface.

(defvar config-exwm--autorandr-profile nil)

(defun config-exwm--autorandr-detect ()
  (split-string (shell-command-to-string "autorandr --detect") "\n" t))

(defun config-exwm-read-autorandr-profile (&optional prompt)
  (unless prompt
    (setf prompt "autorandr profile: "))
  (let ((collection (config-exwm--autorandr-detect)))
    (completing-read prompt collection nil 'require-match)))

(defun config-exwm-load-autorandr-profile (&optional profile force)
  "Load autorandr PROFILE.
If PROFILE is nil, load first profile detected by autorandr.

If optional argument FORCE is non-nil, force reloading of a PROFILE."
  (interactive (list (config-exwm-read-autorandr-profile) current-prefix-arg))
  (unless profile
    (if-let ((detected-profile (car (config-exwm--autorandr-detect))))
        (setf profile detected-profile)
      (user-error "No autorandr profile available")))
  (let ((args (append (if force (list "--force")) (list "--load" profile))))
    (apply #'call-process "autorandr" nil nil nil args))
  (setf config-exwm--autorandr-profile profile)
  (setf exwm-randr-workspace-monitor-plist
        (config-exwm-randr-generate-plist nil))
  (setf exwm-workspace-number (config-exwm-workspace-adjust))
  (exwm-randr-refresh)
  (exwm-workspace-switch 0)
  (message "autorandr profile: %s" profile))

(if (executable-find "autorandr")
    (config-add-one-time-hook 'exwm-randr-screen-change-hook
                              #'config-exwm-load-autorandr-profile))


;;; Polybar.

(declare-function config-my-polybar-start "config-my-polybar")

(add-hook 'exwm-randr-screen-change-hook #'config-my-polybar-start 1)


;;; Keymap: `exwm-mode-map'.

(let ((map exwm-mode-map))
  (keymap-set map "C-q" 'exwm-input-send-next-key))

;;; Footer:

(provide 'config-exwm)

;;; config-exwm.el ends here
