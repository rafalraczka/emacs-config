;;; config-dashboard.el --- Startup screen -*- lexical-binding: t; -*-

;; Copyright (C) 2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

;;;###autoload
(require 'config)

;;;###autoload
(config-install-package 'pico-dashboard)

;;;###autoload
(require 'config-env)

(require 'pico-dashboard)

;;;###autoload
(require 'server)

;;;###autoload
(progn (defun config-pico-dashboard-set-server-footer ()
  (if (server-running-p)
      (setf pico-dashboard-set-footer nil)
    (setf pico-dashboard-footer-messages (list "standalone")))))

;;;###autoload
(add-hook 'after-init-hook #'config-pico-dashboard-set-server-footer)

;;;###autoload
(setf pico-dashboard-banner-logo-title nil)

;;;###autoload
(setf pico-dashboard-items
      '((:description "Bookmark" :key "b" :function bookmark-jump)
        (:description "Mail" :key "m" :function config-mail-dispatch)
        (:description "Project" :key "p" :function config-project-dispatch)))

;;;###autoload
(if config-env-android
    (setf pico-dashboard-image-banner-max-height 50)
  (setf pico-dashboard-image-banner-max-height 200))

;;;###autoload
(let ((command-line-args '(only-one)))
  (pico-dashboard-setup-startup-hook))

;;; Footer:

;;;###autoload
(provide 'config-dashboard)

;;; config-dashboard.el ends here
