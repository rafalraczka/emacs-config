;;; config-keymap.el --- Configuration of keybindings  -*- lexical-binding: t; -*-

;; Copyright (C) 2022-2025 Rafał Rączka <info@rafalraczka.com>

;; Author: Rafał Rączka <info@rafalraczka.com>
;; URL: https://codeberg.org/rafalraczka/emacs-config

;; This file is NOT part of GNU Emacs.

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;; Code:

(require 'transient)


;;; Keymap: `global-map'.

;;;###autoload
(defvar config-keymap-workspace-map-prefix "<f5>")

;;;###autoload
(defvar config-keymap-maps-map-prefix "<f6>")

;;;###autoload
(defvar config-keymap-kmacro-map-prefix "<f7>")

;;;###autoload
(progn
(keymap-global-set "<XF86AudioLowerVolume>" 'desktop-environment-volume-decrement)
(keymap-global-set "<XF86AudioMicMute>" 'desktop-environment-toggle-microphone-mute)
(keymap-global-set "<XF86AudioMute>" 'desktop-environment-toggle-mute)
(keymap-global-set "<XF86AudioNext>" 'desktop-environment-music-next)
(keymap-global-set "<XF86AudioPlay>" 'desktop-environment-toggle-music)
(keymap-global-set "<XF86AudioPrev>" 'desktop-environment-music-previous)
(keymap-global-set "<XF86AudioRaiseVolume>" 'desktop-environment-volume-increment)
(keymap-global-set "<XF86AudioStop>" 'desktop-environment-music-stop)
(keymap-global-set "<XF86Bluetooth>" 'desktop-environment-toggle-bluetooth)
(keymap-global-set "<XF86MonBrightnessDown>" 'desktop-environment-brightness-decrement)
(keymap-global-set "<XF86MonBrightnessUp>" 'desktop-environment-brightness-increment)
(keymap-global-set "<XF86ScreenSaver>" 'desktop-environment-lock-screen)
(keymap-global-set "<XF86WLAN>" 'desktop-environment-toggle-wifi)
(keymap-global-set "<f9>" 'execute-extended-command)
(keymap-global-set "<print>" 'desktop-environment-screenshot)
(keymap-global-set "C-<tab>" nil)
(keymap-global-set "C-x C-j" 'dired-jump)
(keymap-global-set "C-z" nil)
(keymap-global-set "M-<down>" 'transpose-paragraphs)
(keymap-global-set "M-<up>" 'config-my-etc-transpose-paragraphs-previous)
(keymap-global-set "M-u" 'config-my-etc-upcase-previous-word)
(keymap-global-set config-keymap-kmacro-map-prefix 'config-keymap-kmacro-dispatch)
(keymap-global-set config-keymap-maps-map-prefix 'config-keymap-maps-map)
(keymap-global-set config-keymap-workspace-map-prefix 'config-keymap-workspace-map))


;;;; Keymap: `ctl-x-map'.

;;;###autoload
(define-key ctl-x-map (kbd "C-z") nil)


;;; Keymap: 'outline-minor-mode-map'.

(defvar outline-minor-mode-map)

;;;###autoload
(with-eval-after-load 'outline
  (let ((map outline-minor-mode-map))
    (keymap-set map "<backtab>" 'outline-cycle-buffer)
    (keymap-set map "C-<return>" 'outline-insert-heading)
    (keymap-set map "C-<tab>" 'outline-cycle)))


;;; Keymap: `config-keymap-bookmark-map'.

;;;###autoload
(defvar-keymap config-keymap-bookmark-map
  :parent 'bookmark-map)
;;;###autoload
(fset 'config-keymap-bookmark-map config-keymap-bookmark-map)


;;; Keymap: `config-keymap-kmacro-dispatch'.

;;;###autoload (autoload 'config-keymap-kmacro-dispatch "config-keymap" nil t)
(transient-define-prefix config-keymap-kmacro-dispatch ()
  [["Start, end, execute"
    ("s"   "Start recording"              kmacro-start-macro)
    ("C-k" "End or call macro repeat"    kmacro-end-or-call-macro-repeat)
    ("r"   "Apply to lines in region"    apply-macro-to-region-lines)
    ("q"   "kbd-macro-query"             kbd-macro-query)  ;; Like C-x q
    ("d"   "kmacro-redisplay"            kmacro-redisplay)]
   ["Edit"
    ("C-e" "Edit last"                   kmacro-edit-macro-repeat)
    ("RET" "Edit"                        kmacro-edit-macro)
    ("e"   "Edit last"                   edit-kbd-macro)
    ("l"   "Edit recent strokes"         kmacro-edit-lossage)
    ("SPC" "Stop editing"                kmacro-step-edit-macro)]
   ["Naming and binding"
    ("b"   "Bind to key"                 kmacro-bind-to-key)
    ("n"   "Name last"                   kmacro-name-last-macro)
    ("x"   "Store last in register"      kmacro-to-register)]
   ["Ring"
    ("C-n" "Next"                        kmacro-cycle-ring-next)
    ("C-p" "Previous"                    kmacro-cycle-ring-previous)
    ("C-v" "View last"                   kmacro-view-macro-repeat)
    ("C-d" "Delete current"              kmacro-delete-ring-head)
    ("C-t" "Swap first two"              kmacro-swap-ring)
    ("C-l" "Execute second"              kmacro-call-ring-2nd-repeat)]
   ["Macro counter"
    ("C-f" "Set format"                  kmacro-set-format)
    ("C-c" "Set counter"                 kmacro-set-counter)
    ("C-i" "Insert counter"              kmacro-insert-counter)
    ("C-a" "Add counter"                 kmacro-add-counter)]])


;;; Keymap: `config-keymap-project-map'.

;;;###autoload
(defvar-keymap config-keymap-project-map
  "!" 'projectile-run-shell-command-in-root
  "&" 'projectile-run-async-shell-command-in-root
  "4 C-o" 'projectile-display-buffer
  "4 D" 'projectile-dired-other-window
  "4 a" 'projectile-find-other-file-other-window
  "4 b" 'projectile-switch-to-buffer-other-window
  "4 d" 'projectile-find-dir-other-window
  "4 f" 'projectile-find-file-other-window
  "4 g" 'projectile-find-file-dwim-other-window
  "4 t" 'projectile-find-implementation-or-test-other-window
  "5 D" 'projectile-dired-other-frame
  "5 a" 'projectile-find-other-file-other-frame
  "5 b" 'projectile-switch-to-buffer-other-frame
  "5 d" 'projectile-find-dir-other-frame
  "5 f" 'projectile-find-file-other-frame
  "5 g" 'projectile-find-file-dwim-other-frame
  "5 t" 'projectile-find-implementation-or-test-other-frame
  "<left>" 'projectile-previous-project-buffer
  "<right>" 'projectile-next-project-buffer
  "C" 'projectile-configure-project
  "D" 'projectile-dired
  "E" 'projectile-edit-dir-locals
  "ESC" 'projectile-project-buffers-other-buffer
  "F" 'projectile-find-file-in-known-projects
  "I" 'projectile-ibuffer
  "K" 'projectile-package-project
  "L" 'projectile-install-project
  "P" 'projectile-test-project
  "R" 'projectile-regenerate-tags
  "S" 'projectile-save-project-buffers
  "T" 'projectile-find-test-file
  "V" 'projectile-browse-dirty-projects
  "a" 'projectile-find-other-file
  "b" 'projectile-switch-to-buffer
  "c" 'project-compile
  "d" 'projectile-find-dir
  "e" 'projectile-recentf
  "f" 'projectile-find-file
  "g" 'project-find-regexp
  "i" 'projectile-invalidate-cache
  "j" 'projectile-find-tag
  "k" 'projectile-kill-buffers
  "l" 'projectile-find-file-in-directory
  "m" 'projectile-commander
  "o" 'projectile-multi-occur
  "p" 'projectile-switch-project
  "q" 'projectile-switch-open-project
  "r" 'projectile-replace
  "s g" 'projectile-grep
  "s r" 'projectile-ripgrep
  "s s" 'projectile-ag
  "t" 'projectile-toggle-between-implementation-and-test
  "u" 'projectile-run-project
  "v" 'projectile-vc
  "x e" 'projectile-run-eshell
  "x g" 'projectile-run-gdb
  "x i" 'projectile-run-ielm
  "x s" 'projectile-run-shell
  "x t" 'projectile-run-term
  "x v" 'projectile-run-vterm
  "z" 'projectile-cache-current-file)
;;;###autoload
(fset 'config-keymap-project-map config-keymap-project-map)


;;; Keymap: `config-keymap-ref-map'.

;;;###autoload
(defvar-keymap config-keymap-ref-map
  "c" 'citar-copy-reference
  "i" 'citar-insert-citation
  "o" 'citar-open
  "n" 'citar-open-notes
  "l" 'citar-open-links)
;;;###autoload
(fset 'config-keymap-ref-map config-keymap-ref-map)

;;; Keymap: `config-keymap-web-browser-map'.

;;;###autoload
(defvar-keymap config-keymap-web-browser-map
  "f" 'config-my-qutebrowser-switch-to-buffer
  "s" 'config-my-qutebrowser-start-process)
;;;###autoload
(fset 'config-keymap-web-browser-map config-keymap-web-browser-map)


;;; Keymap: `config-keymap-maps-map'.

;;;###autoload
(defvar-keymap config-keymap-maps-map
  "g" 'config-git-map
  "h" 'config-help-map
  "m" 'config-mail-dispatch
  "p" 'config-keymap-project-map
  "r" 'config-keymap-ref-map
  "w" 'config-keymap-web-browser-map)
;;;###autoload
(fset 'config-keymap-maps-map config-keymap-maps-map)


;;; Keymap: `config-keymap-workspace-map'

;;;###autoload
(defvar config-keymap--key-without-super-regexp
  "\\(\\(A-\\)?\\(C-\\)?\\(H-\\)?\\(M-\\)?\\(S-\\)?\\)?\\(.+\\)")

;;;###autoload
(progn (defun config-keymap-workspace-set (keymap key definition)
  (when (key-valid-p key)
    (keymap-set keymap key definition)
    (let ((case-fold-search nil))
      (if (string-match config-keymap--key-without-super-regexp key)
          (keymap-global-set
           (concat (match-string 1 key) "s-" (match-string 7 key))
           definition))))))

(declare-function exwm-workspace-switch-create "ext:exwm-workspace")

;;;###autoload
(defun config-keymap-workspace-switch-index (index)
  (exwm-workspace-switch-create index))

;;;###autoload
(let ((map (define-prefix-command 'config-keymap-workspace-map)))
  (config-keymap-workspace-set map "+" 'balance-windows)
  (dotimes (num 10)
    (config-keymap-workspace-set
     map (number-to-string num)
     `(lambda () (interactive) (tab-bar-select-tab ,(1+ num)))))
  (dotimes (num 10)
    (config-keymap-workspace-set
     map (concat "C-" (number-to-string num))
     `(lambda () (interactive) (config-keymap-workspace-switch-index ,num))))
  (config-keymap-workspace-set map "b" 'switch-to-buffer)
  (config-keymap-workspace-set map "d" 'config-my-etc-delete-window-dwim)
  (config-keymap-workspace-set map "k" 'kill-current-buffer)
  (config-keymap-workspace-set map "l" 'linux-app-launch)
  (config-keymap-workspace-set map "n" 'tab-bar-switch-to-next-tab)
  (config-keymap-workspace-set map "o" 'other-window)

  ;; To use =s-p= while using =gnome= you have to disable system
  ;; shortcut for that keybinding first. It can be done with the
  ;; following command, seems to work on gnome 41.3.
  ;;
  ;; #+begin_src sh
  ;;   $ gsettings set org.gnome.mutter.keybindings switch-monitor '[]'
  ;; #+end_src

  (config-keymap-workspace-set map "p" 'tab-bar-switch-to-prev-tab)

  (config-keymap-workspace-set map "r" 'config-keymap-bookmark-map)
  (config-keymap-workspace-set map "s" 'window-toggle-side-windows)
  (config-keymap-workspace-set map "t" 'config-my-etc-tab-bar-switch-tab-dwim)
  (config-keymap-workspace-set map "w" 'ace-window))

;;; Footer:

(provide 'config-keymap)

;;; config-keymap.el ends here
